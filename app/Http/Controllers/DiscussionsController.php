<?php

namespace App\Http\Controllers;

use App\Discussion;
use Illuminate\Http\Request;

class DiscussionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $discussions = Discussion::all();
        return view('home', ['discussions' => $discussions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Discussion $discussion)
    {
        //
        return view('create_question', compact('discussion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $discussion = new Discussion;
        // $discussion->judul = $request->judul;
        // $discussion->isi = $request->isi;

        // $discussion->save();

        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        Discussion::create($request->all());


        return redirect('/discussions')->with('status', 'Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Discussion  $discussion
     * @return \Illuminate\Http\Response
     */
    public function show(Discussion $discussion)
    {
        //
        return view('view', compact('discussion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Discussion  $discussion
     * @return \Illuminate\Http\Response
     */
    public function edit(Discussion $discussion)
    {
        //
        return view('edit', compact('discussion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discussion  $discussion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discussion $discussion)
    {
        //
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        Discussion::where('id', $discussion->id)
                    ->update([
                        'judul' => $request->judul,
                        'isi' => $request->isi
                    ]);

        return redirect('/discussions')->with('status', 'Success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discussion  $discussion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discussion $discussion)
    {
        //
        
    }
}
