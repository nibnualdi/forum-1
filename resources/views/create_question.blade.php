  @extends('master')

    @section('main')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper pt-5">
    <!-- Main content -->
    <div class="content pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            
            <!-- Card -->
            <!-- PRODUCT LIST -->
            <div class="card card-primary card-outline">

              <!-- /.card-header -->
              <!-- Main content -->
              <div class="content mt-3">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">
                      
                      <!-- Card -->
                      <div class="card">
                        <div class="card-body">
                          <form role="form" method="post" action="/discussions/{{ $discussion->id }}">
                            @csrf
                            <div class="row">
                              <div class="col-sm-12">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>What is the question</label>
                                  <input type="text" class="form-control" placeholder="Title" name="judul">
                                  @error('judul')
                                      <div class="alert alert-danger">{{ $message }}</div>
                                  @enderror
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <!-- textarea -->
                                <div class="form-group">
                                  <label>Description of the question</label>
                                  <textarea class="form-control" rows="3" placeholder="Description" name="isi"></textarea>
                                  @error('isi')
                                      <div class="alert alert-danger">{{ $message }}</div>
                                  @enderror
                                </div>
                              </div>
                            </div>
                            <button type="submit" class="btn btn-secondary btn-sm float-sm-right mr-3">Finish</button>
                          </form>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                      <!-- /card -->

                    </div>
                    <!-- /.col-md-6 -->
                  </div>
                  <!-- /.row -->
                </div><!-- /.container-fluid -->
              </div>
              <!-- /.content -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- /card -->

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection