  @extends('master')

    @section('main')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper pt-5">
    <!-- Main content -->
    <div class="content pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif
            <!-- Card -->
            <!-- PRODUCT LIST -->
            <div class="card">

              <!-- /.card-header -->
              <!-- Main content -->
              <section class="content">
                <div class="container-fluid p-3">
                  <div class="row">
                    <div class="col-12">
                      <!-- Default box -->
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title mt-2 ml-3 text-dark"><small>All Discussions</small></h3>
                          <ol class="breadcrumb bg-transparent float-sm-right mt-0 mb-0">
                            <li class="breadcrumb-item"><a href="/discussions/create">Ask a question</a></li>
                          </ol>
                        </div>
                        @foreach( $discussions as $discussion )
                        <div class="card-body">
                          <div class="card-body p-0">
                            <ul class="products-list product-list-in-card pl-2 pr-2">
                              <li class="item">
                                <div class="product-img">
                                  <img src="{{ asset('/adminlte/dist/img/default-150x150.png') }}" alt="Product Image" class="img-size-50">
                                </div>
                                <div class="product-info">
                                  <a href="/discussions/{{ $discussion->id }}" class="product-title">{{ $discussion->judul }}
                                  </a>
                                  <span class="product-description">
                                    {{ $discussion->isi }}
                                  </span>
                                </div>
                              </li>
                              <!-- /.item -->
                            </ul>
                          </div>
                        </div>
                        @endforeach
                        <!-- /.card-body -->
                        <div class="card-footer">
                          Footer
                        </div>
                        <!-- /.card-footer-->
                      </div>
                      <!-- /.card -->
                    </div>
                  </div>
                </div>
              </section>
              <!-- /.content -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- /card -->

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection