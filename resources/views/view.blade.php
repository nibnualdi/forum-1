  @extends('master')

  @section('main')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <div class="content">
      <div class="container pt-5">
        <div class="row">
          <div class="col-lg-12">
            
            <!-- Card -->
            <div class="card card-primary card-outline mt-5">
              <div class="card-header">
                <h5 class="card-title m-0 mt-1">{{ $discussion->judul }}</h5>
                <button type="button" class="btn btn-primary btn-sm float-right "><a href="{{ $discussion->id }}/edit" class="text-reset">Edit</a></button>
              </div>
              <div class="card-body">

                <p class="card-text">{{ $discussion->isi }}</p>

                <div class="card">
                  <div class="card-header border-0">
                    <h3 class="card-title"> 3 Commetns</h3>
                  </div>
                  <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                      <tbody>
                      <tr>
                        <td>
                          <img src="{{ asset('/adminlte/dist/img/default-150x150.png') }}" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="{{ asset('/adminlte/dist/img/default-150x150.png') }}" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="{{ asset('/adminlte/dist/img/default-150x150.png') }}" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
                <!-- Comment Card -->
                <!-- Card -->
                  <!-- textarea -->
                  <div class="form-group">
                    <label class="ml-2">Your Answer</label>
                    <textarea class="form-control" rows="3" placeholder="Tell your answer here .."></textarea>
                  </div>
                  <button type="button" class="btn btn-secondary btn-sm float-sm-left">Create</button>
                <!-- /card -->
              </div>
            </div>
            <!-- /.card -->
            <!-- /card -->

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection